from flask import Flask
from flask import Markup
from flask import request
from flask import render_template

from process import Analyzer, Statistic
import simulation as sim
import threading

app = Flask(__name__)
analyzer = Analyzer(sim.database)
stats = Statistic()

global simulation_data
global simulation
global sid

@app.route('/')
def index(name='IoT Node Consumption App'):
   return render_template('index.html', name=name)

@app.route('/new_simulation', methods=['POST', 'GET'])
def simulation(name='New Simulation', simulation=None):
    parameters = sim.database.get_all_parameters()
    size = len(parameters)
    if request.method == 'POST':
        result = dict(request.form)
        simulation_data = analyzer.convert_time_units(result)
        if analyzer.check_parameters(result):
            sid = sim.database.get_simulation_next_id()
            sim.database.save_simulation(sid, simulation_data['sim_name'], simulation_data['username'])
            try:
                sim.new_simulation(sid, simulation_data['ports'], simulation_data['node names'], simulation_data['simulation_duration'], simulation_data['sampling_time'], simulation_data['parameters'], len(simulation_data['parameters']))
            except:
                return error(name='Error', message='One or more ports are invalid')
            link = '/results?sid=' + str(sid)
        return render_template('start_sim.html', name='Start Simulation', simulation_data=simulation_data, link=link)
    else:
        return render_template('new_simulation.html', name=name, parameters=parameters, size=size)

@app.route('/start_simulation')
def start_sim(name='Start Simulation', simulation_data=None, ports=None):
    return render_template('start_sim.html', name=name, simulation_data=simulation_data)

@app.route('/database')
def database(name='Database'):
    simulations = sim.database.get_all_simulations()
    nodes = sim.database.get_all_nodes()
    measures = sim.database.get_all_measures()
    parameters = sim.database.get_all_parameters()
    return render_template('database.html', name=name, simulations=simulations,
                            nodes=nodes, measures=measures, parameters=parameters)

@app.route('/results<path:sid>')
def results(name='Results', sid=None):
    sim_data = sim.database.get_simulation(sid)
    summary = stats.get_summary_values(sim_data)
    chart_values = analyzer.separate_values(sim_data)
    vis_data = analyzer.separate_by_nodes_and_prmt(sim_data)
    return render_template('last_sim.html', name=name, summary=summary, limit=chart_values[2],
                            labels=chart_values[0], values=chart_values[1], vis_data=vis_data)


@app.route('/last_sim')
def last_sim(name='Last Simulation Results', sid=None):
    sim_data = sim.database.get_simulation(sim.database.get_simulation_next_id() - 1)
    summary = stats.get_summary_values(sim_data)
    chart_values = analyzer.separate_values(sim_data)
    vis_data = analyzer.separate_by_nodes_and_prmt(sim_data)
    return render_template('last_sim.html', name=name, summary=summary, limit=chart_values[2],
                            labels=chart_values[0], values=chart_values[1], vis_data=vis_data)

@app.route('/history', methods=['POST', 'GET'])
def history(name='Simulation History'):
    simulations = sim.database.get_all_simulations()
    if request.method == 'POST':
        sid_input = request.form
        return results(sid=sid_input)
    return render_template('history.html', name=name, simulations=simulations)

@app.route('/edit_database', methods=['POST', 'GET'])
def edit_database(name='Database Configuration'):
    if request.method == 'POST':
        new_parameter = dict(request.form)
        sim.database.save_parameter(new_parameter['prmt_name'][0], new_parameter['unit'][0])
        return database(name='Database')
    else:
        return render_template('edit_database.html', name='Database Configuration')

@app.route('/setup_database')
def setup_database(name='Database Setup'):
    return render_template('setup_database.html', name=name, message=None)

@app.route('/restore_database')
def restore_database(name='Database Setup'):
    # sim.database.reset_database()
    return render_template('setup_database.html', name=name, message='Database Schema Restored')

@app.route('/create_database')
def create_database(name='Database Setup'):
    try:
        sim.database.create_all_tables()
        message='New Database Schema Created'
    except:
        message='Database Schema Already Created'
        sim.database.conn.rollback()
    return render_template('setup_database.html', name=name, message=message)

@app.errorhandler(404)
def page_not_found(error):
    return render_template('404.html'), 404

@app.route('/500')
def error(name='Error', message=None):
    return render_template('500.html', name='Error')

if __name__ == '__main__':

    app.debug = True
    app.run()
    app.run(debug = True)
