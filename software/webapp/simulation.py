from process import *
from serial import Serial
import serial.tools.list_ports as list_ports
import numpy as np
import db as db
import threading
import datetime
import struct
import time


idLOCK = threading.Lock()
dbLOCK = threading.Lock()
# postgresql database
database = db.Database()


# Function thats receives a list of ports and return a list of serials
def connect_ports(ports, names):
    # Serial reading
    serials = list()
    for port in ports:
        if port != '':
            try:
                srl = Serial(port=port, baudrate=9600)
            except:
                raise Exception('Connection failed with port {}'.format(port))
            connected = False
            while not connected:
                serial_in = srl.read()
                connected = True
            serials.append(srl)
    serials_tuple = [[ports[i], serials[i], names[i]] for i in range(0, len(ports))]
    return serials_tuple

def read_serial_data(sid, datebase, srial, duration, sampling_time, parameters, parameters_qty):
    # Get the serial
    srl = srial[1]
    # Get a new node ID
    nid = database.get_node_next_id()
    # Create a new node
    node = Node(nid, srial[0], srial[2])

    # Send sampling time (in milliseconds) to the node
    srl.write(str(sampling_time).encode())

    data = str()
    current_time = time.time()
    while time.time() < current_time + duration:
        # If the incoming data is compatible with the code

        rx_buffer_data = srl.read().decode("utf-8")
        data += rx_buffer_data
        if(rx_buffer_data == "\n"):
            if len(data.split()) > 0:
                # The process try to split the data according to the number
                # of parameters, ignoring any extra data that arrives
                try:
                    data = data.split()[0:parameters_qty]
                # If the number of data is less than the number of
                # parameters, the 'parameters_qty' is redefined with
                # a new value that is equal to the number of data
                except:
                    data = data.split()
                    parameters_qty = len(data)
                # All the values received a this moment will have the same timestamp
                ts = time.time()
                ts = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S')
                # We use the Node Class to create a list of new measures
                measures = node.save_data(ts, sid, data, parameters, parameters_qty, database, idLOCK, dbLOCK)
                with dbLOCK:
                    for m in measures:
                        # Save each measure in the database
                        database.save_measure(m)
                        print(m)
            data = str()

    # If the locks aren't free, we release them
    if dbLOCK.locked():
        dbLOCK.release()
        print('Database lock released')
    if idLOCK.locked():
        idLOCK.release()
        print('ID lock released')

    print('>> End of simulation')

def read_serials(sid, serials_tuple, duration, sampling_time, parameters, parameters_qty):
    threads = list()
    # Threads creation for each serial
    for tple in serials_tuple:
        serial_thread = threading.Thread(name='Serial Thread {}'.format(serials_tuple.index(tple)), target=read_serial_data, args=(sid, database, tple, duration, sampling_time,parameters, parameters_qty,))
        serial_thread.setDaemon
        threads.append(serial_thread)
        print(serial_thread.name + '... working. Starting time: {}'.format(duration))
    # Start all threads
    for sl_thread in threads:
        sl_thread.start()

def new_simulation(sid, ports, names, duration, sampling_time, parameters, parameters_qty):
    serials_tuple = connect_ports(ports, names)
    threads = read_serials(sid, serials_tuple, duration, sampling_time, parameters, parameters_qty)
