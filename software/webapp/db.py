import psycopg2
import datetime
import time


class Database:

    def __init__(self):
        try:
            # Database connection, requires name, user and password
            self.conn = psycopg2.connect("dbname='postgres' user='iot_db' host='localhost' password='inriachile'")
            self.cursor = self.conn.cursor()
            print('>> Connected to postgres database')
        except:
            return None

    def reset_database(self):
        self.delete_all_tables()
        self.create_all_tables()

    def delete_all_tables(self):
        drop_nodes = 'DROP TABLE nodes'
        drop_measures = 'DROP TABLE measures;'
        drop_simulations = 'DROP TABLE simulations;'
        drop_parameters = 'DROP TABLE parameters;'
        # Execute queries
        self.cursor.execute(drop_measures)
        self.conn.commit()
        self.cursor.execute(drop_measures)
        self.conn.commit()
        self.cursor.execute(drop_simulations)
        self.conn.commit()
        self.cursor.execute(drop_parameters)
        self.conn.commit()

    def create_all_tables(self):
        # Para agregar una columna se debe entregar los siguientes argumentos
        # 1. nombre_dato
        # 2. tipo dato (consultar documentación PostgreSQL)
        # 3. DEFAULT \'---\' (asigna un valor default a una columna caso no se ingrese valor a ella)
        # 4. NOT NULL (opcional, indica si el dado tiene que estar presente o no, en caso que no este, entonces se asigna NULL)
        measure_table = 'CREATE TABLE measures (mid INT NOT NULL, sid INT NOT NULL, nid INT NOT NULL, pid INT NOT NULL, ts TIMESTAMP NOT NULL, value REAL NOT NULL, PRIMARY KEY(mid));'
        simulation_table = 'CREATE TABLE simulations (sid INT NOT NULL, ts TIMESTAMP NOT NULL, name VARCHAR(30) NOT NULL, username VARCHAR(30), PRIMARY KEY(sid));'
        nodes_table = 'CREATE TABLE nodes (nid INT NOT NULL, port VARCHAR(20) NOT NULL, name VARCHAR(15), PRIMARY KEY(nid));'
        parameters_table = 'CREATE TABLE parameters (pid INT NOT NULL, name VARCHAR(20) NOT NULL, unit VARCHAR(15) NOT NULL, PRIMARY KEY(pid))'
        # Ejecutar queries
        self.cursor.execute(measure_table)
        self.conn.commit()
        self.cursor.execute(simulation_table)
        self.conn.commit()
        self.cursor.execute(nodes_table)
        self.conn.commit()
        self.cursor.execute(parameters_table)
        self.conn.commit()

    def add_new_parameter(self, unit):
        columns_query = 'SELECT COUNT(*) FROM INFORMATION_SCHEMA.COLUMNS WHERE table_catalog = \'postgres\' AND table_name = \'measures\';'
        self.cursor.execute(columns_query)
        columns = (int(self.conn.commit()[0][0]) - 4)/2 + 1
        query1 = 'ALTER TABLE measures ADD parameter{0} INT NOT NULL;'.format(coluumns)
        self.cursor.execute(query1)
        self.conn.commit()
        query2 = 'ALTER TABLE measures ADD unit_p{0} VARCHAR(15) DEFAULT \'{1}\';'.format(columns, unit)
        self.cursor.execute(query2)
        self.conn.commit()

    def save_measure(self, m):
        query = 'INSERT INTO measures VALUES ({}, {}, {}, {}, \'{}\', {})'.format(m.mid, m.sid, m.sid, m.pid, m.ts, m.value)
        self.cursor.execute(query)
        self.conn.commit()

    def save_simulation(self, sid, name, username):
        ts = time.time()
        ts = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S')
        query = 'INSERT INTO simulations VALUES ({0}, \'{1}\', \'{2}\', \'{3}\');'.format(sid, ts, name, username)
        self.cursor.execute(query)
        self.conn.commit()

    def save_parameter(self, name, unit):
        pid = self.get_parameter_next_id()
        query = 'INSERT INTO parameters VALUES ({0}, \'{1}\', \'{2}\')'.format(pid, name, unit)
        self.cursor.execute(query)
        self.conn.commit()

    def delete_node(self, nid):
        query = 'DELETE FROM nodes WHERE nid = {0};'.format(nid)
        self.cursor.execute(query)
        self.conn.commit()

    def delete_measure(self, mid):
        query = 'DELETE FROM measures WHERE mid = {0};'.format(mid)
        self.cursor.execute(query)
        self.conn.commit()

    def delete_simulation(self, mid):
        query = 'DELETE FROM simulations WHERE sid = {0};'.format(sid)
        self.cursor.execute(query)
        self.conn.commit()

    def get_all_measures(self):
        query = 'SELECT * FROM measures;'
        self.cursor.execute(query)
        return self.cursor.fetchall()

    def get_all_simulations(self):
        query = 'SELECT * FROM simulations;'
        self.cursor.execute(query)
        return self.cursor.fetchall()

    def get_all_nodes(self):
        query = 'SELECT * FROM nodes;'
        self.cursor.execute(query)
        return self.cursor.fetchall()

    def get_all_parameters(self):
        query = 'SELECT * FROM parameters;'
        self.cursor.execute(query)
        return self.cursor.fetchall()

    def get_data_bydates(self, date1, date2):
        query = 'SELECT * FROM measures WHERE ts BETWEEN \'{}\' AND ts \'{}\';'.format(date1, date2)
        self.cursor.execute(query)
        return self.cursor.fetchall()

    def get_data_by_dates_sid(self, date1, date2, sid):
        query = 'SELECT * FROM measures WHERE ts BETWEEN \'{}\' AND \'{}\' AND sid = {};'.format(date1, date2, sid)
        self.cursor.execute(query)
        return self.cursor.fetchall()

    def search_by_node_and_prmt(self, nid, pid):
        query = 'select * from measures where nid = {} AND pid = {};'.format(nid, pid)
        self.cursor.execute(query)
        return self.cursor.fetchall()

    def search_sid(self, name, user, date1, date2):
        query = 'SELECT * FROM simulations WHERE name = \'{}\' OR username = \'{}\' OR ts BETWEEN \'{}\' AND \'{}\';'.format(name, user, date1, date2)
        self.cursor.execute(query)
        return self.cursor.fetchall()

    def search_parameter(self, pid):
        query = 'SELECT * FROM parameters WHERE pid = {0}'.format(int(pid))
        self.cursor.execute(query)
        return self.cursor.fetchall()[0][1:3]

    def get_simulation(self, sid):
        query = 'SELECT * FROM measures WHERE sid = {} GROUP BY nid, mid ORDER BY ts;'.format(sid)
        self.cursor.execute(query)
        return self.cursor.fetchall()

    def get_data_byusername(self, username):
        query = 'SELECT * FROM simulations WHERE username = {}'.format(username)
        self.cursor.execute(query)
        return self.cursor.fetchall()

    def get_data_bysimulation(self, name):
        query = 'SELECT * FROM simulations WHERE name = {}'.format(username)
        self.cursor.execute(query)
        return self.cursor.fetchall()

    def get_measure_next_id(self):
        query = 'SELECT COUNT(*) FROM measures'
        self.cursor.execute(query)
        return self.cursor.fetchall()[0][0] + 1

    def get_node_next_id(self):
        query = 'SELECT COUNT(*) FROM nodes'
        self.cursor.execute(query)
        return self.cursor.fetchall()[0][0] + 1

    def get_simulation_next_id(self):
        query = 'SELECT COUNT(*) FROM simulations'
        self.cursor.execute(query)
        return self.cursor.fetchall()[0][0] + 1

    def get_parameter_next_id(self):
        query = 'SELECT COUNT(*) FROM parameters'
        self.cursor.execute(query)
        return self.cursor.fetchall()[0][0] + 1

    def __str__(self):
        self.cursor.execute('SELECT * FROM nodes;')
        nodes = self.cursor.fetchall()
        self.cursor.execute('SELECT * FROM measures;')
        measures = self.cursor.fetchall()
        self.cursor.execute('SELECT * FROM simulations;')
        simulations = self.cursor.fetchall()
        return '\nTABLE: NODES\n' + str(nodes) + '\n\n' + 'TABLE: MEASURES\n\n' + str(measures) + '\n\n' + 'TABLE: SIMULATIONS\n\n' + str(simulations)
