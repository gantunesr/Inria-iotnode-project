import csv
import time
import datetime
import statistics


class Analyzer:

    def __init__(self, database):
        self.db = database

    # Function to convert the time input in the correct unit (milliseconds for
    # sampling time and seconds for simulation time), and asign correct data
    # format
    def convert_time_units(self, data):
        result = dict()
        result['simulation_duration'] = int(data['simulation_duration'][0])
        result['sampling_time_measure'] = data['sampling_time_measure'][0]
        result['simulation_time_measure'] = data['simulation_time_measure'][0]
        result['sampling_time'] = int(data['sampling_time'][0])
        result['ports'] = data['ports'][0:len(data['ports']) - 1]
        result['node names'] = data['names'][0:len(data['names']) - 1]
        result['ports_number'] = len(data['ports'])
        result['sim_name'] = data['sim_name'][0]
        result['username'] = data['username'][0]
        result['parameters'] = data['parameters'][0:len(data['parameters'])-1]
        if result['sampling_time_measure'] == 'nanosecs':
            result['sampling_time'] = (10**(-6)) * result['sampling_time']
        elif result['sampling_time_measure'] == 'microsecs':
            result['sampling_time'] = (10**(-3)) * result['sampling_time']
        elif result['sampling_time_measure'] == 'millisecs':
            pass
        elif result['sampling_time_measure'] == 'secs':
            result['sampling_time'] = (10**3) * result['sampling_time']
        if result['simulation_time_measure'] == 'seconds':
            pass
        elif result['simulation_time_measure'] == 'minutes':
            result['simulation_duration'] = 60*result['simulation_duration']
        elif result['simulation_time_measure'] == 'hours':
            result['simulation_duration'] = 60*60*result['simulation_duration']
        elif result['simulation_time_measure'] == 'days':
            result['simulation_duration'] = 60*60*24*result['simulation_duration']
        return result

    def check_parameters(self, result):
        if not result['simulation_duration']:
            return False
        if not result['sampling_time']:
            return False
        if not result['simulation_time_measure']:
            return False
        if not result['sampling_time_measure']:
            return False
        if not result['ports']:
            return False
        if not result['sim_name']:
            return False
        if not result['username']:
            return False
        return True

    def separate_values(self, data):
        dates = [int(time.mktime(i[4].timetuple())) * 1000 for i in data]
        dta = [i[5] for i in data]
        limit = len(dates)
        return [dates, dta, limit]

    def separate_by_nodes_and_prmt(self, data):
        nodes = set()
        prmts = set()
        dates = [int(time.mktime(i[4].timetuple())) * 1000 for i in data]
        data_dict = {'dates':dates, 'data':dict(), 'prmts': dict()}
        for i in data:
            nodes.add(i[2])
            prmts.add(i[3])
        nodes = list(nodes)
        prmts = list(prmts)
        for n in nodes:
            data_dict['data'][str(n)] = dict()
            for p in prmts:
                result = self.db.search_by_node_and_prmt(n, p)
                if len(result) > 0:
                    data_dict['data'][str(n)][str(p)] = [i[5] for i in result]
        for p in prmts:
            data_dict['prmts'][str(p)] = self.db.search_parameter(p)
        return data_dict




class Statistic:

    def __init__(self):
        pass

    def get_summary_values(self, data):
        dta = [i[5] for i in data]
        summary_dict = {'average': statistics.mean(dta),
                        'pvariance': statistics.pvariance(dta),
                        'min': min(dta),
                        'max': max(dta)}
        return summary_dict

    # function to write a csv with the desired simulation
    def write_csv(self, sid, sdate, dates, values, parameters):
        # Values is a list with lists of all the measures by parameter
        lp = len(parameters)
        lv = len(values[0])
        header = ['Dates']
        row = list()
        rows = list()
        for p in parameters:
            header.append('p[0] (p[1])')
        for i in range(0, lv):
            for value_list in values:
                row.append(value_list[i])
            rows.append(row)
            row = [dates[i]]

        with open('./../measures_{}_{}.csv'.format(str(sid), str(sdate), 'w')) as csv_file:
            writer = csv.writer(csv_file, quoting=csv.QUOTE_ALL)
            writer.writerow(header)
            for row in rows:
                writer.writerow(row)

class Measure:

    def __init__(self, mid, sid, nid, pid, ts, value, parameter):
        self.ts = ts
        self.mid = mid
        self.nid = nid
        self.sid = sid
        self.pid = pid
        self.value = value
        self.parameter = parameter

    def __str__(self):
        return  '''Simulation {1} - Node {0} - Measure {2}
                \t>> Timestamp: {3};
                \t>>  Data ({4}): {5} {6};'''.format(self.sid, self.nid, self.mid, self.ts, self.parameter[0], self.value, self.parameter[1])


class Node:

    def __init__(self, nid, port, name):
        self.nid = nid
        self.port = port
        self.name = name
        self.measures = list()

    def save_data(self, ts, sid, data, parameters, qty, database, idLOCK, dbLOCK):
        ms = list()
        with dbLOCK:
            mid = database.get_measure_next_id()
        for i in range(0, qty):
            prmt = database.search_parameter(int(parameters[i]))
            m = Measure(mid, sid, self.nid, parameters[i], ts, data[i], prmt)
            with idLOCK:
                mid += 1
            ms.append(m)
        return ms

    def __str__(self):
        strng = 'Node {0} in port {1} '.format(self.name, self.port)
        for measure in self.measures:
            strng += '\n{}'.str(measure)
        return strng

class Date:

    def __init__(self, year, month, day, hour, minute, second):
        self.year = year
        self.month = month
        self.day = day
        self.hour = hour
        self.minute = minute
        self.second = second

    def __sub__(self, date):
        year = abs(self.year-date.year)*(365*24*60*60)
        month = abs(self.month-date.month)*(30*24*60*60)
        day = abs(self.day-date.day)*(24*60*60)
        hour = abs(self.hour-date.hour)*(60*60)
        minute = abs(self.minute-date.minute)*(60)
        second = abs(self.year-date.year)
        return (year + month + day + hour + minute + second)

    def __add__(self, date):
        year = (self.year+date.year)*365*24*60*60
        month = (self.month+date.month)*30*24*60*60
        day = (self.day+date.day)*24*60*60
        hour = (self.hour+date.hour)*60*60
        minute = (self.minute+date.minute)*60
        second = (self.year+date.year)
        return (year + month + day + hour + minute + second)

    def parse_date(self, date_str):
        # Y Y Y Y - M M - D D     H  H  :  M  M  :  S  S
        # 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18
        return {'year': date_str[0:4],
                'month': date_str[5:7],
                'day': date_str[8:10],
                'hour': date[11:13],
                'minute': date_str[14:16],
                'second': date_str[17:19]}

    def __str__(self):
        return '{}-{}-{} {}:{}:{}'.format(self.year,
                                          self.month,
                                          self.day,
                                          self.hour,
                                          self.minute,
                                          self.second)
