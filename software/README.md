# README #

IoT project for a visualization ui.

## Software and Libraries ##

* Python 3.5
  * os
  * csv
  * time
  * numpy
  * serial
  * pandas
  * psycopg2
  * datetime
  * threading
  * statistics
  * pyserial
  * serial.tools.list_ports
* PostgreSQL 9.6
* Flask 0.12.2

## Source ##

The app is divided into four modules,

1. Main (main.py): The main script. Contains the basic structure of a Flask web app. Each function with the decorator `@app.route(<route>)`, represents a view in the web application.
2. Database (db.py): Script in charge of the database operations, like delete and create tables, execute queries and saves data. The database can be restored if it's desired, this means that all tables will be dropped, therefore, all the data will be lost. After that, empty tables will be created.
3. Process (process.py): Script that contains the classes of different data structures and useful operators. It has a class for the measures and nodes. And a Statistic class that gets the summary values and provides the data for the charts
4. Simulation (simulation.py): Script used to manage the simulation with the help of the threading and serial libraries

## Setup ##

For Ubuntu 16.04 LTS:

### Install PostgreSQL and configure the database ###

Update apt-get,

`sudo apt-get update`

install PostgreSQL,

`sudo apt-get install postgresql postgresql-contrib`

Create new postgres user (for this database we're going to use the user "iot_db" and the password "inriachile", you can change it if you want, just make sure to also change it in the db.py file),

`sudo -u postgres createuser iot_db`

Enter database with postgres user (the postgres user is a full superadmin with access to entire PostgreSQL instance running on your OS),

`sudo -i -u postgres`

Enter console

`psql`

Alter user to superuser,

`ALTER USER iot_db WITH SUPERUSER;`

Set password,

`ALTER USER iot_db WITH PASSWORD 'inriachile';`

Switch role,

`SET ROLE iot_db`

Exit console,

`\q`

Close session,

`exit`

### Install Python3 and all the libraries ###

Check if python3 is already installed,

`python3 --version`

If it returns something like,

`Python 3.5.2`

Then install the libraries, else, execute the following commands,

`sudo apt-get install python3`

To install the libraries, just execute the following command with the desired library,

`sudo python3 -m pip install <library>`

## Running the app ##

Open a new terminal inside the webapp folder and start the server,

`sudo python3 main.py`

Enter the [app](http://127.0.0.1:5000/). If it's the first time you're running the app, then click [here](http://127.0.0.1:5000/setup_database) to create the database schema.

#### Running a new simulation ####

A new simulation requires a serie of inputs like sampling time, duration of the simulation, username, simulation name, simulations parameters and the ports. To run a new simulation enter [here](http://127.0.0.1:5000/new_simulation).

#### Adding a new parameter ####

To add a new parameter, you will need the name and unit oh the parameter. Enter [here](http://127.0.0.1:5000/edit_database) to do it.

#### Simulation history ####

You can see past simulations [here](http://127.0.0.1:5000/history).

## Database Schema ##

As said before, all the data of the app will be stored in a postgresql database. For that the library psycopg2 must be installed, and the using the database, user and password defined previously should be used to setup the connection,

`conn = psycopg2.connect("dbname='postgres' user='iot_db' host='localhost' password='inriachile'")`

### List of relations ###

| Schema |     Name    |  Type |
|:------:|:-----------:|:-----:|
| Public | simulations | Table |
| Public |    nodes    | Table |
| Public |   measures  | Table |
| Public | parameters  | Table |
(4 rows)

### Table: simulations ###

|           | sid (Primary Key) |  ts  |     name    | username    |
|-----------|:-----------------:|:----:|:-----------:|-------------|
| DATA TYPE |        INT        | DATE | VARCHAR(30) | VARCHAR(30) |

### Table: nodes ###

|           | nid (Primary Key) |     port    |     name    |
|-----------|:-----------------:|:-----------:|:-----------:|
| DATA TYPE |        INT        | VARCHAR(20) | VARCHAR(15) |

### Table: measures ###

|           | mid (Primary Key) | sid | nid | pid | ts   | value |
|-----------|:-----------------:|:---:|:---:|-----|------|-------|
| DATA TYPE |        INT        | INT | INT | INT | DATE | REAL  |

### Table: parameters ###

|           | pid (Primary Key) |     name    |     unit    |
|-----------|:-----------------:|:-----------:|:-----------:|
| DATA TYPE |        INT        | VARCHAR(20) | VARCHAR(15) |
