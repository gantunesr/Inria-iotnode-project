from frontend.terminal_ui import TerminalInterface
import backend.simulation as sim
import backend.db as db
import threading
import logging
import time

dbLOCK = sim.dbLOCK
database = sim.database

if __name__ == '__main__':

    ui = TerminalInterface(database, dbLOCK)
    ui.run()
