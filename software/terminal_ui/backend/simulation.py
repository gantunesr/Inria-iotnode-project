from backend.data_structs import Node, Measure
from serial import Serial
import serial.tools.list_ports as list_ports
import backend.db as db
import numpy as np
import threading
import time


idLOCK = threading.Lock()
dbLOCK = threading.Lock()
database = db.Database()



def connect_ports(ports):
    # Serial reading
    serials = list()
    for port in ports:
        try:
            srl = Serial(port=port, baudrate=9600)
            nid = database.get_node_next_id()[0][0] + 1
            node = Node(nid, port)
        except:
            raise Exception('Connection failed with port {}'.format(port))

        connected = False
        while not connected:
            serial_in = srl.read()
            connected = True
        serials.append([srl, node])
    return serials

def read_serial(sid, datebase, srial, duration):
    srl = srial[0]
    node = srial[1]
    with dbLOCK:
        mid = database.get_measure_next_id()[0][0] + 1

    data = str()
    current_time = time.time()
    while time.time() < current_time + duration:
        rx_buffer_data = srl.read().decode("utf-8")
        data += rx_buffer_data
        if(rx_buffer_data == "\n"):
            print(data)
            if len(data.split()) == 3:
                data = data.split()
                measure = node.save_data(mid, sid, data[0], data[1])
                with dbLOCK:
                    print(measure)
                    # database.save_measure(measure)
                with idLOCK:
                    mid += 1
            data = str()

def read_serials(sid, serials, duration):
    threads = list()
    for serial in serials:
        serial_thread = threading.Thread(name='Serial Thread {}'.format(serials.index(serial)), target=read_serial, args=(sid, database, serial, duration,))
        serial_thread.setDaemon
        threads.append(serial_thread)
        print(serial_thread.name + '... working. Starting time: {}'.format(duration))
    for sl_thread in threads:
        sl_thread.start()

def new_simulation(sid, ports, duration):
    serials = connect_ports(ports)
    threads = read_serials(sid, serials, duration)
