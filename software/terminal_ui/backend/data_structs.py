import time
import datetime
import statistics


class Plotter:
    pass


class Statistic:

    def __init__(self):
        pass

    def get_summary_values(self, data):
        voltage1 = [i[4] for i in data]
        voltage2 = [i[5] for i in data]
        summary_dict = {'vol1_average': statistics.mean(voltage1),
                        'vol2_average': statistics.mean(voltage2),
                        'vol1_pvariance': statistics.pvariance(voltage1),
                        'vol2_pvariance': statistics.pvariance(voltage2),
                        'vol1_min': min(voltage1),
                        'vol2_min': min(voltage2),
                        'vol1_max': max(voltage1),
                        'vol2_max': max(voltage2)}
        return summary_dict


class Measure:

    def __init__(self, mid, nid, sid, voltage1, voltage2):
        ts = time.time()
        self.ts = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S')
        nmid = self.ts.replace('-', '').replace(':', '').replace(' ', '')
        self.mid = mid
        self.nid = nid
        self.sid = sid
        self.voltage1 = voltage1
        self.voltage2 = voltage2

    def __str__(self):
        return  '''Node {0} - Simulation {1} - Measure {2}
                \t>> Timestamp: {3};
                \t>> Voltage 1: {4};
                \t>> Voltage 2: {5};'''.format(self.nid, self.sid, self.mid, self.ts, self.voltage1, self.voltage2)


class Node:

    def __init__(self, nid, kind):
        self.nid = nid
        self.kind = kind
        self.measures = list()

    def save_data(self, mid, sid, voltage1, voltage2):
        new = Measure(mid, self.nid, sid, voltage1, voltage2)
        return new

    def __str__():
        strng = 'Node {0} ({1})'.format(str(self.nid), self.kind)
        for measure in self.measures:
            strng += '\n{}'.str(measure)
        return strng

class Date:

    def __init__(self, year, month, day, hour, minute, second):
        self.year = year
        self.month = month
        self.day = day
        self.hour = hour
        self.minute = minute
        self.second = second

    def __sub__(self, date):
        year = abs(self.year-date.year)*(365*24*60*60)
        month = abs(self.month-date.month)*(30*24*60*60)
        day = abs(self.day-date.day)*(24*60*60)
        hour = abs(self.hour-date.hour)*(60*60)
        minute = abs(self.minute-date.minute)*(60)
        second = abs(self.year-date.year)
        return (year + month + day + hour + minute + second)

    def __add__(self, date):
        year = (self.year+date.year)*365*24*60*60
        month = (self.month+date.month)*30*24*60*60
        day = (self.day+date.day)*24*60*60
        hour = (self.hour+date.hour)*60*60
        minute = (self.minute+date.minute)*60
        second = (self.year+date.year)
        return (year + month + day + hour + minute + second)

    def parse_date(self, date_str):
        # Y Y Y Y - M M - D D     H  H  :  M  M  :  S  S
        # 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18
        return {'year': date_str[0:4],
                'month': date_str[5:7],
                'day': date_str[8:10],
                'hour': date[11:13],
                'minute': date_str[14:16],
                'second': date_str[17:19]}

    def __str__(self):
        return '{}-{}-{} {}:{}:{}'.format(self.year,
                                          self.month,
                                          self.day,
                                          self.hour,
                                          self.minute,
                                          self.second)
