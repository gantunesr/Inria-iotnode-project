from PyQt5 import QtCore, QtGui, QtWidgets, uic
from backend.data_structs import Statistic
import csv
import PyQt5
import os.path


stats = Statistic()

main_window = uic.loadUiType('/home/guto/Projects/Inria/Program/src/frontend/ui/main_window.ui')
simulation_widget = uic.loadUiType('/home/guto/Projects/Inria/Program/src/frontend/ui/simulation.ui')

class Signal(QtCore.QObject):
    # Esta clase define la nueva señal escribe_señal
    simulation_s = QtCore.pyqtSignal()


class SimulationWidget(simulation_widget[0], simulation_widget[1]):

    def __init__(self, db):
        super().__init__()
        self.setupUi(self)

        self.start_button.clicked.connect(self.start)
        self.stop_button.clicked.connect(self.stop)
        self.csv_button.clicked.connect(self.generate_csv)
        self.charts_button.clicked.connect(self.generate_charts)

        self.db = db
        self.sim_results = None
        self.sim_id = 0

    def write_measure_table(self, measure):
        row = self.measure_table.rowCount()
        self.measure_table.insertRow(row)
        self.measure_table.setItem(row, 0, QtWidgets.QTableWidgetItem(str(measure[2])))
        self.measure_table.setItem(row, 1, QtWidgets.QTableWidgetItem(str(measure[1])))
        self.measure_table.setItem(row, 2, QtWidgets.QTableWidgetItem(str(measure[4]) + ' Volts'))
        self.measure_table.setItem(row, 3, QtWidgets.QTableWidgetItem(str(measure[5]) + ' Volts'))

    def write_summary_table(self, data):
        # Average v1
        self.summary_table.setItem(0, 0, QtWidgets.QTableWidgetItem(str(data['vol1_average'])))
        # Average v2
        self.summary_table.setItem(1, 0, QtWidgets.QTableWidgetItem(str(data['vol2_average'])))
        # Min v1
        self.summary_table.setItem(0, 1, QtWidgets.QTableWidgetItem(str(data['vol1_min']) + ' Volts'))
        # Min v2
        self.summary_table.setItem(1, 1, QtWidgets.QTableWidgetItem(str(data['vol2_min']) + ' Volts'))
        # Max v1
        self.summary_table.setItem(0, 2, QtWidgets.QTableWidgetItem(str(data['vol1_max']) + ' Volts'))
        # Max v2
        self.summary_table.setItem(1, 2, QtWidgets.QTableWidgetItem(str(data['vol2_max']) + ' Volts'))

    def generate_csv(self):
        if self.sim_results:
            with open('./../measures/measures_sim{}.csv'.format(self.sim_id), 'w') as csv_file:
                print('New csv file ./../measures/measures_sim{}.csv'.format(self.sim_id))
                writer = csv.writer(csv_file, quoting=csv.QUOTE_ALL)
                writer.writerow(['Date', 'Voltage 1', 'Voltage 2'])
                for measure in self.sim_results:
                    writer.writerow([str(measure[2]), measure[4], measure[5]])

    def generate_charts(self):
        if self.sim_results:
            stats.simple_plot(self.sim_results)

    def start(self, duration):
        for rb_id in ['secondsrb', 'minutesrb', 'hoursrb', 'daysrb']:
            if getattr(self, rb_id).isChecked():
                option = getattr(self, rb_id).text()
                if option == 'Seconds':
                    duration = int(self.duration_input.text())
                elif option == 'Minutes':
                    duration = int(self.duration_input.text())*60
                elif option == 'Hours':
                    duration = int(self.duration_input.text())*60**2
                elif option == 'Days':
                    duration = int(self.duration_input.text())*24*60**2
        if duration:
            self.sim_results = self.db.simulation(duration)
            for measure in self.sim_results:
                self.write_measure_table(measure)
            summary = stats.get_values(duration, self.sim_results)
            self.write_summary_table(summary)


        self.sim_id += 1


    def error_popup(self):
        pass

    def stop(self):
        pass

class MainWindow(main_window[0], main_window[1]):

    def __init__(self):
        super().__init__()
        self.setupUi(self)

        # Button actions
        self.simulation_button.clicked.connect(self.new_simulation_window)

    def new_simulation_window(self):
        window = SimulationWidget()
        window.show()
        self.close()

    def write_measure_table(self, measure):
        row = self.measure_table.rowCount()
        self.measure_table.insertRow(row)
        self.measure_table.setItem(row , 0, QtWidgets.QTableWidgetItem(measure.ts))
        self.measure_table.setItem(row , 1, QtWidgets.QTableWidgetItem(measure.nid))
        self.measure_table.setItem(row , 2, QtWidgets.QTableWidgetItem(str(measure.voltage) + ' Volts'))
        self.measure_table.setItem(row , 3, QtWidgets.QTableWidgetItem(str(measure.power) + ' Volts'))


class GUI:

    def __init__(self, database, dbLOCK, uiLOCK):
        self.database = database
        self.dbLOCK = dbLOCK
        self.uiLOCK = uiLOCK

        self.app = QtWidgets.QApplication([])
        self.window = SimulationWidget(database)

    def write_measure(self, measure):
        self.window.write_measure_table(measure)

    def new_simulation(self, duration):
        self.window.start(duration)

    def run(self):
        print('Interface initialized')
        self.window.show()
        self.app.exec_()
