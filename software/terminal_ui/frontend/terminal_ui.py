from backend.data_structs import Statistic
import csv
import time
import datetime
import os.path
import backend.simulation as sim


class TerminalInterface():

    error = False

    def __init__(self, database, dbLOCK):
        self.dbLOCK = dbLOCK
        self.database = database
        self.plt_measures = list()
        self.sampling = 0
        self.stats = Statistic()

    def start_simulation_menu(self):
        qnodes = int(input('Number of nodes to use: '))
        ports = list()
        print('Enter ports, e.g., /dev/ttyACM0')
        for i in range(0, qnodes):
            ports.append(input('Port #{}: '.format(i)))
        error = True
        while error:
            error = False
            print('\nSampling time options: ')
            print('1 - Seconds')
            print('2 - Milliseconds')
            print('3 - Microsconds')
            print('4 - Nanoseconds')
            time_system = int(input('Enter time unit option to use (1, 2, 3 or 4): '))
            sampling = int(input('Enter sampling time: '))
            if time_system == 1:
                self.sampling = sampling*(10**9)
            elif time_system == 2:
                self.sampling = sampling*(10**6)
            elif time_system == 3:
                self.sampling = sampling*(10**3)
            elif time_system == 4:
                self.sampling = sampling
            else:
                error = True

            # Send sampling time to node
            # ...code
            # ...code

        return ports

    def simple_plot(self, table_data):
        stats.simple_plot(table_data)

    def write_csv(self, table_data, date1, date2):
        dates = list()
        voltage1 = list()
        voltage2 = list()
        for measure in table_data:
            dates.append(data[3])
            voltage1.append(data[4])
            voltage2.append(data[5])
        with open('./../measures_{}_{}.csv'.format(str(date1), str(date2)), 'w') as csv_file:
            writer = csv.writer(csv_file, quoting=csv.QUOTE_ALL)
            writer.writerow(['Date', 'Voltage 1', 'Voltage 2'])
            for d, v1, v2 in zip(date, voltage1, voltage2):
                writer.writerow([d, v1, v2])

    def summary_table(self, table, date1, date2):
        summary = self.stats.get_summary_values(table)
        average_str = 'AVERAGE  | {} | {} |'.format(summary['vol1_average'], summary['vol2_average'])
        space = int((len(average_str)-10)/4)
        header_str = '         | VOLTAGE 1 | VOLTAGE 2 |'
        print('\n\nFROM {} TO {}'.format(date1, date2))
        print(header_str)
        print('-'*len(average_str))
        print(average_str)
        print('VARIANCE | {} | {} |'.format(summary['vol1_pvariance'], summary['vol2_pvariance']))
        print('MIN      | {} | {} |'.format(summary['vol1_min'], summary['vol2_min']))
        print('MAX      | {} | {} |'.format(summary['vol1_max'], summary['vol2_max']))

    def menu(self):
        print('\n\nSelect one option\n')
        print('1 - Simulation')
        print('2 - Data base')
        print('3 - ')
        print('  - Exit')

        inpt = int(input('>> '))
        if inpt == 1:
            self.simulation_menu()
        elif inpt == 2:
            self.database_menu()
        elif inpt == 3:
            self.history_menu()
        else:
            return

    def simulation_menu(self):
        print('\n == SIMULATION MENU ==')
        print('\n\nSelect one option\n')
        print('1 - Start new simulation')
        print('2 - Simulation history')
        print('  - Exit')

        inpt = int(input('>> '))
        if inpt == 1:
            sim_tuple = self.new_simulation()
            self.results_menu(sim_tuple[0], sim_tuple[1], sim_tuple[2])
        elif inpt == 2:
            self.history_menu()
        else:
            return

    def results_menu(self, current_time, duration, sid):
        date1 = datetime.datetime.fromtimestamp(current_time).strftime('%Y-%m-%d %H:%M:%S')
        date2 = datetime.datetime.fromtimestamp(current_time + duration).strftime('%Y-%m-%d %H:%M:%S')
        print(date1)
        print(date2)
        print(sid)
        table_results = self.database.get_data_by_dates_sid(date1, date2, sid)
        self.summary_table(table_results, date1, date2)

    def duration_menu(self):
        return

    def new_simulation(self):
        print('\n == NEW SIMULATION ==\n')
        ports = self.start_simulation_menu()
        sid = self.database.get_next_simulation_id()[0][0] + 1
        print('\n\nEnter simulation name')
        simulation_name = input('>> ')
        print('Enter username')
        username = input('>> ')
        error = True
        while error:
            # Get duration simulation
            error = False
            print('Enter time unit:')
            print('1 - Seconds')
            print('2 - Minutes')
            print('3 - Hours')
            print('4 - Days')
            time_system = int(input('Enter time unit option to use (1, 2, 3 or 4): '))
            duration = int(input('Enter the duration of the simulation: '))
            if time_system == 1:
                pass
            elif time_system == 2:
                duration = duration*60
            elif time_system == 3:
                duration = duration*60*60
            elif time_system == 4:
                duration = duration*60*60*24
            else:
                error = True

        print('\n\nNew simulation started...')
        # Save simulation in database
        self.database.save_simulation(sid, simulation_name, username)
        # Starting new simulation
        sim.new_simulation(sid, ports, duration)
        # Current time
        current_time = time.time()
        # Inactive
        time.sleep(duration)
        # Return tuple -> current_time, duration, sid
        return (current_time, duration, sid)

    def database_menu(self):

        error = True
        while error:
            # Get duration simulation
            error = False
            print('\nDatabase Menu')
            print('>> Select one option')
            print('1 - See all measures')
            print('2 - See all nodes')
            print('3 - See all simulations')
            print('4 - Search measure')
            print('5 . Search node')
            print('5 . Search simulation')

            inpt = int(input('>> '))
            if inpt == 1:
                pass


    def history_menu(self):
        print('\nEnter username (omissible)')
        username = input('>> ')
        print('Enter simulation name (omissible)')
        name = input('>> ')
        print('Enter dates where the simulation occured (not omissible)')
        print('format: YYY-MM-DD HH:MM:SS')
        print('\nEnter init Date')
        date1 = input('>> ')
        print('\nEnter end Date')
        date2 = input('>> ')
        with self.dbLOCK:
            simulations = self.database.search_sid(name, username, date1, date2)
        if len(simulations) > 0:
            print('All simulations founded according to the parameters')
            for i in simulations:
                print('Simulation ID: {} - Timestamp: {} - Simulation Name: {} - Username: {}'.format(i[0], i[1], i[2], i[3]))
            print('\n\nSelect Simulation ID')
            sid = int(input('>> '))
            simulation = self.database.get_simulation(sid)
        else:
            print('No simulation founded...')


    def gui(self):
        while True:
            self.menu()


    def run(self):
        self.gui()


    def __str__(self):
        pass
