// Constants
int SENSOR_PIN1 = A0;  // Input pin for measuring Vout
int SENSOR_PIN2 = A1;  // Input pin for measuring Vout
const int RS = 10;          // Shunt resistor value (in ohms)
const float VOLTAGE_REF = 4.99;  // Reference voltage for analog read
const int ELSE = 0;
int dly = 1000;

// Global Variables
float sensorValue1;   // Variable to store value from analog read
float current1;       // Calculated current value
float sensorValue2;   // Variable to store value from analog read
float current2;       // Calculated current value
//float current2_B;

void setup() {
  
  // Initialize serial monitor
  Serial.begin(9600);
  
}

void loop() {
  if (Serial.available() > 0){
    dly = Serial.readString().toInt();
    }
  
  // Read a value from the INA169 board
  sensorValue1 = analogRead(SENSOR_PIN1);
  sensorValue2 = analogRead(SENSOR_PIN2);
  
  sensorValue1 = (sensorValue1 * VOLTAGE_REF) / 1023;
  sensorValue2 = (sensorValue2 * VOLTAGE_REF) / 1023;

  current1 = sensorValue1*100 / RS;
  current2 = sensorValue2*100 / RS;
  current2 = -2.4885*pow(current2,4) + 54*pow(current2,3) - 385.35*pow(current2,2) + 1172*current2 - 351.4;
  if (current1 > 1.9){
    Serial.println(current1,4);
    }
  else if (current2 < 2000 && current2 > 0){
    Serial.println(current2/1000,6);
    }
  else if (current2 < 0){
    Serial.println(ELSE);
    }

  delay(dly);
  
}
